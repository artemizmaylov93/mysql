# Version: 0.0.1
FROM ubuntu:14.04
RUN apt-get update \
        && DEBIAN_FRONTEND=noninteractive apt-get install -y mysql-server

ADD start.sh /
RUN chmod +x /start.sh

CMD [ "/start.sh" ]